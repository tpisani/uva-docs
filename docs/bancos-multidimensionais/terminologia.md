Terminologia do Paradigma Dimensional
=====================================

*27 de fevereiro de 2015*


### Dimensão

Conjunto de dados de um campo armazando (coluna).

![Dimensão](img/bancos-multidimensionais/dimensao.png)


### Agregação

Análise conjunta de duas ou mais dimensões de forma a gerar uma solução
para um determinado problema. Uma das dimensões deve estar, necessariamente,
relacionada ao aspecto temporal.


### Fato

São formados pelo conjunto de dimensões utilizadas para tentar solucionar um
determinado problema. No exemplo anterior, as dimensões *região*, *produto* e
*mês* compõem a *tabela de fatos*.


### Medidas

São cálculos obtidos através da análise dos valores individuais de dimensões.

---


O paradigma relacional é amplamente consolidado desde a década de 1970 para
aplicações de banco de dados em geral, em especial para aquelas onde há um
grau elevado de atualização dos dados. Isto se dá em razão da distribuição
dos dados em diferentes entidades, característica essa que privilegia as
instruções de escrita, que representam a maior quantidades de problemas
relativos à consistência dos dados armazenados.


No entanto, a partir do fim da década de 1990, a necessidade do uso de grandes
volumes de dados, em tempo hábil, para a tomada de decisões *vem causando a
evolução* de um paradigma com **características analíticas** denominado **dimensional**.


Paradigma dimensional está associado à forma como os dados são estruturados
de maneira a tornar os processos de **agregação** mais eficientes. Tais processos são a chave da boa utilização deste paradigma como ferramenta de auxílio à tomada de decisões.


Uma vez que o emprego do paradigma dimensional está voltado para situações analíticas, torna-se necessário agregar os dados armazenados na forma de **dimensões**. Cada dimensão está *associada a um campo armazenado*. Por fim, é necessário a definição *obrigatória* de uma **dimensão temporal** para que uma solução analítica seja posta em prática.
