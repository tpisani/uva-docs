UVA Docs
========

O UVA Docs tem como objetivo armazenar, através do trabalho colaborativo, referências
importantes e o conteúdo das disciplinas cursadas na **Universidade Veiga de Almeida**,
para que qualquer aluno possa **consultar e estudar de forma fácil e rápida**.

---


Como contribuir
---------------

Contribuições podem ser feitas através de *issues* com anotações anexadas (texto ou imagem)
ou *pull-requests*. Caso esteja interessado, você pode me procurar e solicitar acesso ao
repositório. Fique a vontade para colaborar!

Fique atento aos requisitos que as anotações devem atender, eles tornam o trabalho
de incorporar e organizar as anotações muito mais fácil.


### Dados necessários para inclusão de anotações

- **Nome da disciplina** *(e código se possível)*

- **Nome do professor**

- **Data da anotação**

- **Anotação legível e organizada ;)**


### Ferramentas utilizadas

O UVA Docs utiliza o [MkDocs](http://www.mkdocs.org/), baseado na marcação
[Markdowm](https://guides.github.com/features/mastering-markdown/), caso você
não esteja familiarizado com esses nomes é aconselhável que você dê uma olhada
nesses links.
