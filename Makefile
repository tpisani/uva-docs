install:
	@pip install -r requirements.txt


run:
	@mkdocs serve


build:
	@mkdocs build --clean
